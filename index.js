// console.log("Hello World!");

// [SECTION] - Functions
// Functions in JS are lines/blocks of codes that will tell our device/application to perform a particulat task.
// Functions are mostly created to creat complicated tasks to run several lines of code succession.
// They are also used to prevent repeating line of codes.

// Function Declaration -> 

function printGrade(){
	let grade1 = 90;
	let grade2 = 95;
	let ave = (grade1 + grade2)/2;

	console.log(ave);
}

// calling or invoke/onvokation
printGrade();

// [SECTION] Function Declaration vs Expression

// This is a sample function declaration
function declaredFunction(){
	console.log("Hello World from declaredFunction()");
}

declaredFunction();

// This is a function expression
// A function can also be stored in a variable. This is called function expression.
// Anonymous function - a function without a name.

// variableFunction();
/*
	error - function expressions being stored
	in a let or const, cannot be hoisted.
*/

// Anonymous function
// Function Expression Example
let variableFunction = function(){
	console.log("Hello Again!");
}

variableFunction();

let functionExpression = function funcName(){
	console.log("Hello from the other side");
}

functionExpression(); //This is the right invocation in function expression
// funcName(); --> This will return error

// You can re-assign declared functions and function expression to a new anonymour function

declaredFunction = function(){
	console.log("updated declaredFunction()");
}

declaredFunction();

functionExpression = function(){
	console.log("updated functionExpression()");
}

functionExpression();

// However, we cannot re-assign a function expression initialized with const. 

const constantFunction = function(){
	console.log("Initialized with const!");
}

constantFunction();

/*	
	constantFunction = function(){
		console.log("Will try to re-assign");
	}
		
	constantFunction();
*/

// [SECTION] - Funtion Scoping
/*
1. Local/block Scope
2. Global Scope
3. Function Scope
*/

{
	// This is a local variable and its value is only accessible inside the curly braces.
	let localVariable = "Armando Perez";
}

// This is a global variable and its value is accessible anywhere in the code base.
let globalVariable = "Mr. WorldWide";

console.log(globalVariable);

function showNames(){
	// Function scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();

/*	
	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

	This will return error since the 3 variables are stored in a function.
*/

// Nested Function

function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName ="John";
		// child function can inherit data from parent function
		console.log(nestedName);
		console.log(name);
	}
	nestedFunction();
	// console.log(nestedName); -> error/function scoped
}

myNewFunction();
// nestedFunction(); -> result will be error

// Function and Global Scoped Variables

let globalName = "Alexandro"

function myNewFunction2(){
	let nameInside = "Renz"

	console.log(globalName);
	console.log(nameInside);
}

myNewFunction2();
// console.log(nameInside); -> will return an error

// [SECTION] - Using Alert
// alert() allows us to show small window at the top of our browser.

// alert("Hello World");

function showSampleAlert(){
	alert("Hello User!");
}

showSampleAlert();

console.log("I will only log in the console when alert is dismissed");

// Notes on the use of alert()
	// Show only alert() for short dialog message.
	// Do not overuse alert() because progrom/js has to wait for it to be dismissed before continuing.

// [SECTION] - Using prompt()
// prompt() allows us to show a small window and gather user input.
// usually prompt are stored in a variable.

// let samplePrompt = prompt("Enter your Name.");

// console.log("Hello, " + samplePrompt);
// console.log(typeof samplePrompt);

// let sampleNullPrompt = prompt("Do not enter anything.");

// console.log(sampleNullPrompt); // returns an empty string

function printWelcomeMessage(){
	let firstName = prompt("Enter your first Name.");
	let lastName = prompt("Enter your last name");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}

printWelcomeMessage()

// [SECTION] - Function Naming Convention
//  Function names should be definitive of the task it will perform. It usually contains verb.

function getCourse(){
	let courses = ["Science 101", "Math 101", "English 101"]; 
	console.log(courses);
}

getCourse();

// Avoid generic names to avoid confusion within your code.

function get(){
	let name = "Jamie";
	console.log(name);
}

get();